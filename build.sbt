name := "compred"

organization := "com.tritcorp.exp"

version := "1.0"

scalaVersion := "2.12.4"

scalacOptions += "-target:jvm-1.8"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "3.1.0",
  "org.apache.spark" %% "spark-sql" % "3.1.0",
  "org.apache.spark" %% "spark-hive" % "3.1.0",
  "org.apache.spark" %% "spark-mllib" % "3.1.0",
  "com.typesafe" % "config" % "1.4.0",
  "com.github.haifengl" %% "smile-scala" % "2.5.3",
  "com.github.haifengl" % "smile-plot" % "2.5.3",
  "com.thoughtworks.xstream" % "xstream" % "1.4.13",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
)


test in assembly := {}

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case _ => MergeStrategy.first

}

