package com.tritcorp.compred

import com.thoughtworks.xstream.XStream
import com.thoughtworks.xstream.io.xml.StaxDriver
import com.tritcorp.compred.FilesUtils._
import com.tritcorp.compred.Utils._
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, functions}
import org.apache.spark.storage.StorageLevel
import smile.data.formula.Formula
import smile.read.parquet
import smile.regression.RandomForest
import smile.validation.{mad, rmse}

import java.io._
import java.util.Locale

object Compred extends LazyLogging {

  Locale.setDefault(Locale.ENGLISH)


  import ss.implicits._


  def enrich_trips(ds: DataFrame): DataFrame = {

    val enriched_ds = ds
      .withColumn("period", period2Num($"period"))
      .withColumn("holidays", holidays($"trip_start"))


    osm_enriched.join(enriched_ds, Seq("stop_orig", "stop_dest", "line", "chaining")).distinct
      .withColumn("effective_speed_tmp", speedCalcFloat($"length", $"travel_time", lit("-1.0")))
      .withColumn("effective_speed", when($"effective_speed_tmp" <= 0, $"effective_speed").otherwise($"effective_speed_tmp"))
      .drop("effective_speed_tmp")
      .selectExpr("line_type", "cast(stop_orig as integer)", "cast(stop_dest as integer)", "legal_speed", "length", "nb_traffic_signals", "nb_crossings", "nb_stops",
        "nb_give_ways", "nb_lvl_crossings", "proportion_bus_lane", "proportion_bike_on_road", "proportion_one_way", "proportion_slow_zone", "nb_roundabouts",
        "effective_speed as real_speed", "dwell_time as dt", "period", "day", "holidays", "sm", "trip_start", "order_orig")
      .orderBy("sm", "trip_start", "order_orig")
  }

  def enrich(ds: DataFrame, highspeed_lim: Int = 71, lowspeed_lim: Int = 9): DataFrame = {

    val enriched_ds = ds.where($"effective_speed" < highspeed_lim and $"effective_speed" > lowspeed_lim and $"deviation" === false and $"travel_time" > 0 and $"dwell_time" >= 0)
      .withColumn("period", period2Num($"period"))
      .withColumn("holidays", holidays($"trip_start"))


    osm_enriched.join(enriched_ds, Seq("stop_orig", "stop_dest", "line", "chaining")).distinct
      .drop("effective_speed")
      .withColumn("effective_speed", speedCalcFloat($"length", $"travel_time", lit("-1.0")))
      .where($"effective_speed" > 0)
      .distinct
      .selectExpr("line_type", "cast(stop_orig as integer)", "cast(stop_dest as integer)", "legal_speed", "length", "nb_traffic_signals", "nb_crossings", "nb_stops",
        "nb_give_ways", "nb_lvl_crossings", "proportion_bus_lane", "proportion_bike_on_road", "proportion_one_way", "proportion_slow_zone", "nb_roundabouts", "effective_speed as real_speed", "dwell_time as dt", "period", "day", "holidays" /*, "month"*/)

  }

  def main(args: Array[String]): Unit = {


    /** ***************************************************************************************************************
      * CONFIGURATION
      * ************************************************************************************************************** */

    /** Change to true to train a new model. will take at least 8 hours on a 4 cores x86 system
      * BEWARE : Existing model will be overwritten */
    val TRAIN_MODEL = true

    /** CONFIG */
    val line = "969"
    val chaining = "21"
    val direction = "R"
    val points = 11
    val remove_osm_features = true

    /** ***************************************************************************************************************
      * START OF APP CODE
      * ************************************************************************************************************** */
    //Used for outputs naming. Do not touch that
    val suffix = if (remove_osm_features) "_nf" else ""

    /**
      * Used to keep the micro predictions order
      */


    val reorder = historical.where($"line" === line and $"chaining" === chaining and $"direction" === direction)
      .selectExpr("stop_orig", "stop_dest", "key", "order_orig")
      .distinct.orderBy("order_orig")
    logger.warn("\n---------------------------------------------------------")
    logger.warn(s"CURRENT CONFIG : ")
    logger.warn(s"line: $line")
    logger.warn(s"direction: $direction")
    logger.warn(s"chaining: $chaining")
    logger.warn(s"number of inter-stops of the line: $points")
    logger.warn(s"dropping OSM features: $remove_osm_features")
    logger.warn("\n---------------------------------------------------------\n\n\n")

    logger.warn("Preprocessing data...")

    val ips_test = historical.where($"line" === line and $"chaining" === chaining and $"direction" === direction).select("key", "order_orig").distinct.orderBy("order_orig").collect.map(_.getAs[String](0))

    val train_raw_ds = historical.where(!($"line" === line and $"key".isin(ips_test: _*) and $"direction" === direction))

    val already_known_ips = train_raw_ds.where($"key".isin(ips_test: _*)).select("key").distinct.collect.map(_.getAs[String](0))
    val unknown_ips = ips_test.diff(already_known_ips)

    val test_trip = historical.where($"line" === line and $"chaining" === chaining and $"direction" === direction and
      $"effective_speed" < 55 and $"deviation" === false and $"incomplete" === false and $"travel_time" > 0 and $"dwell_time" >= 0)

    val trips_data = test_trip.selectExpr("stop_orig", "stop_dest", "line",
      "chaining", "effective_speed ", "travel_time", "dwell_time", "period", "day", "order_orig", "trip_start", "sm", "incomplete")
      .orderBy($"trip_start", $"sm", $"order_orig")

    val valid_trips = trips_data.groupBy("sm", "trip_start", "period").agg(count("*").as("tot")).where($"tot" === points).distinct.drop("tot")

    val test_trip_base = test_trip.join(valid_trips, Seq("sm", "trip_start", "period"))

    val test_raw_ds_known = test_trip_base.where($"key".isin(already_known_ips: _*))
    val test_raw_ds_unknown = test_trip_base.where($"key".isin(unknown_ips: _*))

    val train_ds = enrich(train_raw_ds, 55).persist(StorageLevel.fromString("DISK_ONLY"))

    logger.warn(s"Train dataset size (number of tuples) ${train_ds.count}\n")

    /** NOTE that taking only 10% of the 15M tuples dataset for training will yield results that will be quite identical to those observed in the paper. Moreover, the computing time and memory usage will be way diminished
      * If there is no model training, just take a sample of data in order to keep the code on working without impacting perf. much.
      * train is needed for model metadata display (features importance, etc)
      */
    if (TRAIN_MODEL)
      train_ds/*.sample(0.1)*/.coalesce(1).write.mode("overwrite").format("org.apache.spark.sql.parquet").save(s"$workDir/train")
    else
      train_ds.sample(0.0001).coalesce(1).write.mode("overwrite").format("org.apache.spark.sql.parquet").save(s"$workDir/train")

    train_ds.unpersist()

    lazy val test_ds_known = enrich_trips(test_raw_ds_known).cache
    lazy val test_ds_unknown = enrich_trips(test_raw_ds_unknown).cache

    logger.warn(s"known inter-stops : ${already_known_ips.size}, unknown inter-stops : ${unknown_ips.size}, total : ${ips_test.size}\n")

    val holidays = Array(1, 2, 3, 4, 5, 6)
    val days = Array(0, 1, 2, 3, 4, 5, 6)

    /** TRAINING MODELS
      * */
    var f = new File(s"$workDir/train")
    var tree = getFileTree(f)
    val train = parquet(getAllFilesEndingWith(tree, ".parquet").head.getPath)

    System.gc()

    /** RANDOM FOREST REGRESSOR */
    val xreg: smile.data.DataFrame = if (!remove_osm_features) {
      train.drop("stop_orig", "stop_dest")
    } else {
      train.drop("stop_orig", "stop_dest", "legal_speed", "length", "nb_traffic_signals", "nb_crossings", "nb_stops",
        "nb_give_ways", "nb_lvl_crossings", "proportion_bus_lane", "proportion_bike_on_road", "proportion_one_way", "proportion_slow_zone", "nb_roundabouts")
    }

    val xstream = new XStream(new StaxDriver())
    // GRID SEARCH
    /*
        val results:ListBuffer[(String,Double,Double)] = ListBuffer[(String,Double,Double)]()

        val hp = new Hyperparameters()
          .add("smile.random.forest.trees", 50, 750, 25) // a fixed value
          .add("smile.random.forest.mtry", Array(2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)) // an array of values to choose
          .add("smile.random.forest.max.nodes", 100, 500, 50) // range [100, 500] with step 50
          .add("smile.random.forest.max.depth", 2, 30, 2)

        val train1 = xreg.slice(0, (xreg.size() * 0.8).toInt)
        val test1 = xreg.slice((xreg.size() * 0.8).toInt + 1, xreg.size)
        val formula = Formula.lhs("real_speed")
        val testy = formula.y(test1).toDoubleArray()

        hp.grid().parallel().forEach{prop =>
          val model = RandomForest.fit(formula, train1, prop)
          val pred = model.predict(test1)
          results.append((prop.toString,model.error(),rmse(testy, pred)))
    /*      println(prop)
          println("OOB RMSE = %.2f".format(model.error()))
          println("RMSE = %.2f".format(rmse(testy, pred)))*/
        }

        println(results.toList.minBy(_._3))
        println(results.toList.minBy(_._2))

    */
    val rf: RandomForest = if (TRAIN_MODEL) {
      if (!remove_osm_features)
        smile.regression.randomForest(Formula.lhs("real_speed"), xreg, ntrees = 250, mtry = 15, maxDepth = 24, maxNodes = 500)
      else
        smile.regression.randomForest(Formula.lhs("real_speed"), xreg)
    }
    else {
      logger.warn("Loading model...\n")
      val ois = new ObjectInputStream(new FileInputStream(s"""$workDir/rf_model_${line}_$direction${suffix}"""))
      val xml = ois.readObject().asInstanceOf[String]
      val model = xstream.fromXML(xml).asInstanceOf[RandomForest]
      ois.close()
      model
    }
    System.gc()

    // NOT USED
    // cf Arbitrated Ensemble for Time Series Forecasting @ https://link.springer.com/chapter/10.1007/978-3-319-71246-8_29
    // Ensemble methods error prediction (Can it predict error prediction of RF given the original ds ?)
    /*
    val pred = rf.predict(xreg)
    val xregTruth = xreg.select("real_speed").toArray.flatten
    val err = xregTruth.zip(pred).map(v => v._1 - v._2)
    val errVec = DoubleVector.of("err", err)
    val resdf: smile.data.DataFrame = smile.data.DataFrame.of(errVec)
    val xregerr: smile.data.DataFrame = xreg.merge(resdf).drop("real_speed")
    val nModel = smile.regression.randomForest(Formula.lhs("err"), xregerr, ntrees = 250, mtry = 15, maxDepth = 24, maxNodes = 500)
    val nmodelOOB = nModel.error()
    println(nmodelOOB)
*/
    if (TRAIN_MODEL) {
      logger.warn("Training model...\n")
      val oos = new ObjectOutputStream(new FileOutputStream(s"""$workDir/rf_model_${line}_$direction${suffix}"""))
      val xml = xstream.toXML(rf)
      oos.writeObject(xml)
      oos.close()
    }

    logger.warn(s"Ouf of bag RMSE: ${rf.error()}")
    logger.warn(s"Features importance: ")
    logger.warn("\n")
    logger.warn(s"\n${xreg.drop("real_speed").names.zip(rf.importance).sortBy(_._2).mkString("\n")}")
    logger.warn("\n")
    System.gc()

    logger.warn(s"WORKING ON LINE $line CHAINING $chaining DIRECTION $direction\n")

    holidays.foreach { holiday =>
      days.foreach { day =>
        Periods.foreach { period =>

          logger.warn("----------------------------------")
          logger.warn(s"HOLIDAYS $holiday DAY $day PERIOD $period")

          val found_known = test_ds_known.where($"day" === day and $"holidays" === holiday and $"period" === period)
            .select("stop_orig", "stop_dest")
            .distinct.count

          val found_unknown = test_ds_unknown.where($"day" === day and $"holidays" === holiday and $"period" === period)
            .select("stop_orig", "stop_dest")
            .distinct.count

          try {
            assert(already_known_ips.length == found_known)
            assert(unknown_ips.length == found_unknown)
            assert(unknown_ips.length + already_known_ips.length == ips_test.length)

            test_ds_known
              .where($"day" === day and $"holidays" === holiday and $"period" === period)
              .coalesce(1).write.mode("overwrite")
              .format("org.apache.spark.sql.parquet")
              .save(s"$workDir/test_known")

            f = new File(s"$workDir/test_known")
            tree = getFileTree(f)
            val tests_known_base = parquet(getAllFilesEndingWith(tree, ".parquet").head.getPath)
            val tests_known = if (!remove_osm_features) {
              tests_known_base
                .drop("stop_orig", "stop_dest", "sm", "trip_start", "order_orig")
            } else {
              tests_known_base
                .drop("stop_orig", "stop_dest", "sm", "trip_start", "order_orig", "legal_speed", "length",
                  "nb_traffic_signals", "nb_crossings", "nb_stops", "nb_give_ways", "nb_lvl_crossings", "proportion_bus_lane",
                  "proportion_bike_on_road", "proportion_one_way", "proportion_slow_zone", "nb_roundabouts")
            }

            test_ds_unknown
              .where($"day" === day and $"holidays" === holiday and $"period" === period)
              .coalesce(1).write.mode("overwrite")
              .format("org.apache.spark.sql.parquet")
              .save(s"$workDir/test_unknown")
            f = new File(s"$workDir/test_unknown")
            tree = getFileTree(f)
            val tests_unknown_base = parquet(getAllFilesEndingWith(tree, ".parquet").head.getPath)

            val tests_unknown = if (!remove_osm_features) {
              tests_unknown_base
                .drop("stop_orig", "stop_dest", "sm", "trip_start", "order_orig")
            } else {
              tests_unknown_base
                .drop("stop_orig", "stop_dest", "sm", "trip_start", "order_orig", "legal_speed", "length",
                  "nb_traffic_signals", "nb_crossings", "nb_stops", "nb_give_ways", "nb_lvl_crossings", "proportion_bus_lane",
                  "proportion_bike_on_road", "proportion_one_way", "proportion_slow_zone", "nb_roundabouts")
            }

            test_ds_known.unpersist()
            test_ds_unknown.unpersist()

            logger.warn(s"train dataset size : ${train.size()}")
            logger.warn(s"test known dataset size : ${tests_known.size()}")
            logger.warn(s"test unknown dataset size : ${tests_unknown.size()}")

            val preds_reg_known = tests_known_base("real_speed")
            val lengths_known = tests_known_base("length")
            val orig_known = tests_known_base("stop_orig")
            val dest_known = tests_known_base("stop_dest")
            val dc_known = tests_known_base("trip_start")
            val sm_known = tests_known_base("sm")

            val preds_reg_unknown = tests_unknown_base("real_speed")
            val lengths_unknown = tests_unknown_base("length")
            val orig_unknown = tests_unknown_base("stop_orig")
            val dest_unknown = tests_unknown_base("stop_dest")
            val dc_unknown = tests_unknown_base("trip_start")
            val sm_unknown = tests_unknown_base("sm")

            val results_k = rf.predict(tests_known)
            val rmse_k = rmse(results_k, preds_reg_known.toDoubleArray)
            val mae_k = mad(results_k, preds_reg_known.toDoubleArray)
            //val resCorrk: Array[Double] = nModel.predict(tests_known).zip(results_k).map{ v =>v._2 + v._1}

            logger.warn(s"RMSE TEST KNOWN: $rmse_k km/h")
            logger.warn(s"MAE TEST KNOWN: $mae_k km/h")

            /*logger.warn(s"RMSE CORRECTED KNOWN: ${rmse(resCorrk, preds_reg_known.toDoubleArray)}")
            logger.warn(s"MAE CORRECTED KNOWN: ${mad(resCorrk, preds_reg_known.toDoubleArray)}")*/

            val results_u = rf.predict(tests_unknown)
            val rmse_u = rmse(results_u, preds_reg_unknown.toDoubleArray)
            val mae_u = mad(results_u, preds_reg_unknown.toDoubleArray)
            //val resCorru: Array[Double] = nModel.predict(tests_unknown).zip(results_u).map{ v =>v._2 + v._1}

            logger.warn(s"RMSE TEST UNKOWN: $rmse_u km/h")
            logger.warn(s"MAE TEST UNKOWN: $mae_u km/h")

            /*            logger.warn(s"RMSE CORRECTED UNKOWN: ${rmse(resCorru, preds_reg_unknown.toDoubleArray)}")
                        logger.warn(s"MAE CORRECTED UNKOWN: ${mad(resCorru, preds_reg_unknown.toDoubleArray)}")*/


            val analk = preds_reg_known.toDoubleArray()
              .zip(results_k)
              .zip(orig_known.toIntArray())
              .zip(dest_known.toIntArray())
              .zip(lengths_known.toDoubleArray())
              .zip(dc_known.toStringArray())
              .zip(sm_known.toStringArray())
              .map { x => (x._1._1._1._1._1._1, x._1._1._1._1._1._2, x._1._1._1._1._2, x._1._1._1._2, x._1._1._2, x._1._2, x._2) }.toSeq.toDF("truth", "pred", "stop_orig", "stop_dest", "length", "trip_start", "trip_service")
              .withColumn("known", lit(true))
              .withColumn("rmse", lit(rmse_k))
              .withColumn("mae", lit(mae_k))
              .cache

            val analu = preds_reg_unknown.toDoubleArray()
              .zip(results_u)
              .zip(orig_unknown.toIntArray())
              .zip(dest_unknown.toIntArray())
              .zip(lengths_unknown.toDoubleArray())
              .zip(dc_unknown.toStringArray())
              .zip(sm_unknown.toStringArray())
              .map { x => (x._1._1._1._1._1._1, x._1._1._1._1._1._2, x._1._1._1._1._2, x._1._1._1._2, x._1._1._2, x._1._2, x._2) }.toSeq.toDF("truth", "pred", "stop_orig", "stop_dest", "length", "trip_start", "trip_service")
              .withColumn("known", lit(false))
              .withColumn("rmse", lit(rmse_u))
              .withColumn("mae", lit(mae_u))
              .cache

            val res = analk.union(analu)
              .join(reorder, Seq("stop_orig", "stop_dest"))
              .orderBy("trip_start", "trip_service", "order_orig")


            res
              .coalesce(1).write.mode("overwrite").option("header", "true")
              .format("com.databricks.spark.csv")
              .save(s"$workDir/OUT/${line}_${chaining}_${direction}_${ips_test.head.split("-").head}-${ips_test.last.split("-").last}${suffix}/holidays=${holiday}/day=${day}/period=${period}")

            analk.unpersist()
            analu.unpersist()
          }
          catch {
            case e: AssertionError => {
              logger.error("[ASSERTION ERROR]")
              logger.error(s"FOR HOLIDAYS $holiday DAY $day PERIOD ${period} - ${NumPeriod(period)}")
              logger.error(s"NO TRIPS DATA / INCOMPLETE TRIPS / COMPOSITION IMPOSSIBLE\n")

            }
            case e: Exception => {
              logger.error("[EXCEPTION]")
              logger.error(s"CANNOT DO STUFF FOR HOLIDAYS $holiday DAY $day PERIOD ${period} - ${NumPeriod(period)}\n\n")
              logger.error(s"REASON : Probably no data for the given time period\n")
            }
          }
        }
      }
    }

    logger.warn("----------------------------------")
    logger.warn("--           ML ENDED           --")
    logger.warn("----------------------------------")

    logger.warn("Computing / aggregating results")
    /** AGGREGATING SPEEDS TO COMPARE MACRO SPEED PREDICTIONS */

    //Adding String version of period for charts readability
    val compred = ss.read.option("header", "true")
      .format("com.databricks.spark.csv")
      .load(s"""$workDir/OUT/${line}_${chaining}_${direction}_${ips_test.head.split("-").head}-${ips_test.last.split("-").last}${suffix}""")
      .withColumn("pd", num2Period($"period"))
      .selectExpr("stop_orig", "stop_dest", " cast(truth as double)", "cast(pred as double)", "cast(length as double)",
        "known", "rmse", "mae", "key", "cast(order_orig as integer)", "trip_start", "trip_service", "cast(holidays as integer)", "cast(day as integer)", "cast(period as integer)", "pd")

    val micro_avg = compred.groupBy("stop_orig", "stop_dest", "length", "key", "known", "order_orig", "holidays", "day", "period", "pd")
      .agg(avg("truth").as("truth"), avg("pred").as("pred"))
      .orderBy("holidays", "day", "period", "order_orig")

    micro_avg
      .coalesce(1).write.mode("overwrite").option("header", "true")
      .format("com.databricks.spark.csv")
      .save(s"""$workDir/OUT/${line}_${chaining}_${direction}_${ips_test.head.split("-").head}-${ips_test.last.split("-").last}_pd_avg${suffix}""")

    compred.coalesce(1).write.mode("overwrite").option("header", "true")
      .format("com.databricks.spark.csv")
      .save(s"""$workDir/OUT/${line}_${chaining}_${direction}_${ips_test.head.split("-").head}-${ips_test.last.split("-").last}_pd${suffix}""")

    val compred_timed = compred.withColumn("time_truth", $"length" / ($"truth" / 3.6))
      .withColumn("time_pred", $"length" / ($"pred" / 3.6))


    var begin = true
    holidays.foreach { h =>
      days.foreach { d =>
        Periods.foreach { p =>

          var write_mode = if (begin) "overwrite" else "append"

          /** COMPUTE average prediction against average reality for each period: Average compred */
          val mavg_timed = micro_avg.where($"holidays" === h and $"day" === d and $"period" === p)
            .withColumn("time_truth", $"length" / ($"truth" / 3.6))
            .withColumn("time_pred", $"length" / ($"pred" / 3.6)).cache

          val mavg_sample = mavg_timed

          val mavg_t_max_truth = mavg_sample.agg(functions.max($"time_truth")).collect().head.getAs[Double](0)
          val mavg_t_max_pred = mavg_sample.agg(functions.max($"time_pred")).collect().head.getAs[Double](0)

          val mavg_sample1 = mavg_sample
            .withColumn("p_pred", $"time_pred" / mavg_t_max_pred)
            .withColumn("p_truth", $"time_truth" / mavg_t_max_truth)

          val mavg_sample2 = mavg_sample1
            .withColumn("weighed_pred", $"pred" * $"p_pred")
            .withColumn("weighed_truth", $"truth" * $"p_truth")
            .cache

          val mavg_truth = mavg_sample2
            .agg((functions.sum("weighed_truth") / functions.sum("p_truth")).as("true_speed"))
            .withColumn("holidays", lit(h))
            .withColumn("day", lit(d))
            .withColumn("period", lit(p))

          val mavg_pred = mavg_sample2
            .agg((functions.sum("weighed_pred") / functions.sum("p_pred")).as("pred_speed"))
            .withColumn("holidays", lit(h))
            .withColumn("day", lit(d))
            .withColumn("period", lit(p))

          mavg_pred
            .join(mavg_truth, Seq("holidays", "day", "period"))
            .where($"pred_speed".isNotNull and $"true_speed".isNotNull)
            .withColumn("pd", num2Period($"period"))
            .withColumn("err", functions.abs($"true_speed" - $"pred_speed"))
            .coalesce(1)
            .write.mode(write_mode).option("header", "true")
            .format("com.databricks.spark.csv")
            .save(s"""$workDir/OUT/compred_${line}_${chaining}_${direction}_avg${suffix}""")

          mavg_sample.unpersist
          mavg_sample2.unpersist

          val period_sample = compred_timed.where($"holidays" === h and $"day" === d and $"period" === p).cache
          val trips = period_sample.select("trip_service", "trip_start").distinct.collect.map { x => (x.getAs[String](0), x.getAs[String](1)) }

          /** Computing compred using each trip : averaging after computing trips independently and comparing with each trip real speed */
          trips.foreach { trip =>
            write_mode = if (begin) "overwrite" else "append"
            begin = false
            val sample = period_sample.where($"trip_service" === trip._1 and $"trip_start" === trip._2)
            period_sample.unpersist()

            val t_max_truth = sample.agg(functions.max($"time_truth")).collect().head.getAs[Double](0)
            val t_max_pred = sample.agg(functions.max($"time_pred")).collect().head.getAs[Double](0)
            val sample1 = sample.withColumn("p_pred", $"time_pred" / t_max_pred)
              .withColumn("p_truth", $"time_truth" / t_max_truth)
            val sample2 = sample1.withColumn("weighed_pred", $"pred" * $"p_pred")
              .withColumn("weighed_truth", $"truth" * $"p_truth").cache
            val truth = sample2.agg((functions.sum("weighed_truth") / functions.sum("p_truth")).as("true_speed"))
              .withColumn("holidays", lit(h)).withColumn("day", lit(d)).withColumn("period", lit(p))
            val pred = sample2.agg((functions.sum("weighed_pred") / functions.sum("p_pred")).as("pred_speed"))
              .withColumn("holidays", lit(h)).withColumn("day", lit(d)).withColumn("period", lit(p))

            pred.join(truth, Seq("holidays", "day", "period"))
              .where($"pred_speed".isNotNull and $"true_speed".isNotNull)
              .withColumn("pd", num2Period($"period"))
              .withColumn("err", functions.abs($"true_speed" - $"pred_speed"))
              .coalesce(1)
              .write.mode(write_mode).option("header", "true").format("com.databricks.spark.csv")
              .save(s"""$workDir/OUT/compred_${line}_${chaining}_${direction}_raw${suffix}""")
            sample.unpersist
            sample2.unpersist
          }

        }
      }
    }

    ss.read.option("header", "true").format("com.databricks.spark.csv")
      .load(s"$workDir/OUT/compred_${line}_${chaining}_${direction}_raw${suffix}")
      .groupBy("holidays", "day", "period", "pd")
      .agg(avg("pred_speed").as("pred_speed"), avg("true_speed").as("true_speed"))
      .coalesce(1)
      .write.mode("overwrite").option("header", "true")
      .format("com.databricks.spark.csv")
      .save(s"""$workDir/OUT/compred_${line}_${chaining}_${direction}${suffix}""")

    ss.stop()

    logger.warn("DONE\n")
    logger.warn(s"Everything was saved @ $workDir/OUT/\n")
    if (TRAIN_MODEL) logger.warn(s"""MODEL SAVED: $workDir/rf_model_${line}_$direction${suffix}""")
    logger.warn(s"Micro learning Raw (useful for stats): ${line}_${chaining}_${direction}_${ips_test.head.split("-").head}-${ips_test.last.split("-").last}${suffix}")
    logger.warn(s"Micro learning Raw with period as string (useful for stats): ${line}_${chaining}_${direction}_${ips_test.head.split("-").head}-${ips_test.last.split("-").last}_pd_avg${suffix}")
    logger.warn(s"Micro learning, averaged grouped by inter-stop, holidays, day, period (Useful for charts): ${line}_${chaining}_${direction}_${ips_test.head.split("-").head}-${ips_test.last.split("-").last}_pd${suffix}")
    logger.warn(s"Compositional (Compred) results, trip by trip (useful for stats): compred_${line}_${chaining}_${direction}${suffix}")
    logger.warn(s"Compositional result grouped by holidays,day,period. its the average speed of each trip of each period.(Useful for charts that compares with Macro results): compred_${line}_${chaining}_${direction}_raw${suffix}")
    logger.warn(s"Average Compred. Its the average speed for each holidays,day,period. Based on the average speed of inter-points  (Useful for charts  BUT, should not be used for comparison with Macro): compred_${line}_${chaining}_${direction}_avg${suffix}")

  }


}
