package com.tritcorp.compred

import java.util.concurrent.ThreadLocalRandom._
import com.typesafe.scalalogging.LazyLogging
import smile.stat.distribution.GaussianDistribution
import smile.data.DataFrame
import smile.data.formula.Formula
import scala.collection.mutable.ArrayBuffer
import smile.regression.RandomForest
import smile.validation
import org.apache.log4j.BasicConfigurator

object Mock extends Enumeration with LazyLogging {
  type Agregation = Value
  val SUM, PROD = Value


  BasicConfigurator.configure()
  val rootLogger = org.apache.log4j.Logger.getRootLogger()
  rootLogger.setLevel(org.apache.log4j.Level.toLevel("INFO"))

  /** *
    * Internal classes, traits, etc
    */

  protected trait Turns2Array {
    def toArray: Either[Array[Double], Array[Array[Double]]]
  }


  /** Foo data class */
  protected case class Data(
                             day: Double,
                             holidays: Double,
                             period: Double,
                             target: Double
                           ) extends Turns2Array {
    def toArray: Either[Array[Double], Array[Array[Double]]] = {
      Left(Array(day, holidays, period, target))
    }


  }

  /** Defines an inter-stop directly at instantiation.
    * If needed, change the parameters directly in the case class declaration */
  protected case class InterStop(
                                  length: Double = current().nextDouble(150, 600),
                                  legal_speed: Double = current().nextDouble(30, 55),
                                  nb_traffic_signals: Double = current.nextInt(0, 3).toDouble,
                                  nb_crossings: Double = current.nextInt(0, 4).toDouble,
                                  nb_stops: Double = if (current.nextDouble(0, 1).toDouble > 0.8) 1.0 else 0.0,
                                  nb_giveways: Double = current.nextInt(0, 3).toDouble,
                                  nb_lvl_crossings: Double = if (current.nextDouble(0, 1) > 0.95) 1.0 else 0.0,
                                  nb_roundabouts: Double = current.nextInt(0, 2).toDouble,
                                  prop_bike: Double = current().nextDouble(0, 1),
                                  prop_bus_lane: Double = current().nextDouble(0, 1),
                                  prop_slow_zone: Double = current().nextDouble(0, 1),
                                  prop_one_way: Double = current().nextDouble(0, 1)) extends Turns2Array {

    def toArray: Either[Array[Double], Array[Array[Double]]] = {
      Left(Array(length, legal_speed, nb_traffic_signals, nb_crossings, nb_stops, nb_giveways,
        nb_lvl_crossings, nb_roundabouts, prop_bike, prop_bus_lane, prop_slow_zone, prop_one_way))
    }

  }

  /** Defines a line */
  protected case class Line(id: Int, interStops: Array[InterStop]) extends Turns2Array {
    def toArray: Either[Array[Double], Array[Array[Double]]] = {
      val its = interStops.map(i => Array(id.toDouble) ++ i.toArray.left.get)
      Right(its)
    }

  }

  /**
    * Defines a bus trip for a given bus line
    *
    * @param line
    * @param data
    * @param id
    */
  protected case class Trip(line: Line, data: Array[Data], id: Double) extends Turns2Array {

    override def toArray: Either[Array[Double], Array[Array[Double]]] = {
      val line_array = line.toArray.right.get
      val output = new ArrayBuffer[Array[Double]]
      for (i <- line_array.indices) {
        output.append(line_array(i) ++ data(i).toArray.left.get ++ Array(id))
      }
      Right(output.toArray)
    }

    def toMicroArray: DataFrame = {
      createDF(toArray.right.get)
    }

    def toMacroArray(agg_mode: Value = SUM): DataFrame = {
      val micro = toMicroArray

      val line = micro("line_id").toDoubleArray().head
      val length: Double = micro("length").toDoubleArray().sum
      val legal_speed: Double = micro("legal_speed").toDoubleArray().sum.toDouble / micro.size()
      val nb_traf: Double = micro("nb_traffic_signals").toDoubleArray().sum
      val nb_x: Double = micro("nb_crossings").toDoubleArray().sum
      val nb_s: Double = micro("nb_stops").toDoubleArray().sum
      val nb_g: Double = micro("nb_giveways").toDoubleArray().sum
      val nb_lvl: Double = micro("nb_lvl_crossings").toDoubleArray().sum
      val nb_ra: Double = micro("nb_roundabouts").toDoubleArray().sum
      val prop_bike: Double = micro("prop_bike").toDoubleArray().sum.toDouble / micro.size()
      val prop_bus_lane: Double = micro("prop_bus_lane").toDoubleArray().sum.toDouble / micro.size()
      val prop_slow_zone: Double = micro("prop_slow_zone").toDoubleArray().sum.toDouble / micro.size()
      val prop_one_way: Double = micro("prop_one_way").toDoubleArray().sum.toDouble / micro.size()
      val day = micro("day").toDoubleArray.head
      val holidays = micro("holidays").toDoubleArray.head
      val period = micro("period").toDoubleArray.head
      val target = if (agg_mode != PROD) micro("target").toDoubleArray().sum else micro("target").toDoubleArray().product
      val trip_id = micro("trip_id").toDoubleArray().head

      val pre_data = Array(Array(line, length, legal_speed, nb_traf, nb_x, nb_s, nb_g, nb_lvl, nb_ra, prop_bike, prop_bus_lane, prop_slow_zone, prop_one_way, day, holidays, period, target, trip_id))

      createDF(pre_data)

    }
  }

  protected case class Metrics(rmse: Double, mae: Double, mape: Double, pct_known_iStops: Double) {
    override def toString: String = {
      s"RMSE: ${rmse - (rmse % 0.001)}, MAE: ${mae - (mae % 0.001)}, MAPE: ${mape - (mape % 0.001)} %, " +
        s"Percentage of known inter-stops: ${(pct_known_iStops * 100) - ((pct_known_iStops * 100) % 0.001)} %"
    }
  }


  /**
    * Generates a travel time for the inter-stop iStop based on its features and the holidaus value, type of day and period in the day
    *
    * @param iStop
    * @param day
    * @param holiday
    * @param period
    * @return
    */
  protected def travelTime(iStop: InterStop, day: Double, holiday: Double, period: Double): Double = {
    val avg_speed_ms = 35 * Math.exp(-1 * (0.03 * day + 0.02 * holiday + 0.05 * period + 0.0002 * iStop.length + 0.002 * iStop.legal_speed +
      0.001 * iStop.nb_crossings + 0.0002 * iStop.nb_giveways + 0.0003 * iStop.nb_roundabouts + 0.007 * iStop.nb_stops + 0.01 * iStop.nb_traffic_signals +
      0.0002 * iStop.prop_bike - 0.0001 * iStop.prop_bus_lane + 0.0001 * iStop.prop_one_way + 0.0002 * iStop.prop_slow_zone)) / 3.6

    val avg_tt = iStop.length / avg_speed_ms

    /** normal law used to generate foo values considering the values are normally distributed
      * May be used to generate reliability values
      * UNUSED AT THE MOMENT */
    val norm = new GaussianDistribution(avg_tt, current().nextDouble(0.025, 0.15) * avg_tt)
    Math.abs(norm.rand())
  }

  /** Generates a travel reliability for the inter-stop ip based on its features and the holidaus value, type of day and period in the day
    *
    * @param iStop
    * @param day
    * @param holiday
    * @param period
    * @return
    */
  protected def reliability(iStop: InterStop, day: Double, holiday: Double, period: Double): Double = {
    val rel = 1 * Math.exp(-1 * (0.00002 * day + 0.00002 * holiday + 0.000035 * period + 0.000002 * iStop.length + 0.000005 * iStop.legal_speed +
      0.000003 * iStop.nb_crossings + 0.000005 * iStop.nb_giveways + 0.000003 * iStop.nb_roundabouts + 0.000002 * iStop.nb_stops + 0.000005 * iStop.nb_traffic_signals +
      0.000002 * iStop.prop_bike - 0.000001 * iStop.prop_bus_lane + 0.000001 * iStop.prop_one_way + 0.000002 * iStop.prop_slow_zone))

    val norm = new GaussianDistribution(rel, current().nextDouble(0.01, 0.05) * rel)
    val res = Math.abs(norm.rand())
    if (res > 1) 1.0 else if (res < 0) 0.0 else res

  }

  /**
    * Creates virtual trips using a set of existing lines. For each of them it creates a random number of trips delimited by
    * min_trips_per_line and max_trips_per_line
    * */
  protected def generateTrips(lines: Array[Line], min_trips_per_line: Int = 20, max_trips_per_line: Int = 100,
                              targetGen: (InterStop, Double, Double, Double) => Double): Array[Trip] = {

    /** Trips buffer */
    val trips = new ArrayBuffer[Trip]()

    lines.foreach { l =>

      /** Generate n trips with min_trips_per_line <= n < max_trips_per_line */
      for (i <- 0 until current().nextInt(min_trips_per_line, max_trips_per_line)) {
        val data = new ArrayBuffer[Data]()

        /**
          * generate days:  0 = weekend, 1 = week day
          * generate holidays: 0 = holidays, 1 = no holidays
          * generate period: 0 = free, 1 = during day, 2 = rush hour
          */
        val day = current.nextInt(0, 1).toDouble
        val holiday = current.nextInt(0, 1).toDouble
        val period = current.nextInt(0, 2).toDouble

        l.interStops.foreach { is =>

          data.append(Data(day, holiday, period, Math.abs(targetGen(is, day, holiday, period))))
        }
        trips.append(Trip(l, data.toArray, i.toDouble))
      }
    }
    trips.toArray
  }

  /** Used to create the ML dataframe */
  protected def createDF(data: Array[Array[Double]]): DataFrame = {
    DataFrame.of(data, "line_id", "length", "legal_speed", "nb_traffic_signals",
      "nb_crossings", "nb_stops", "nb_giveways", "nb_lvl_crossings", "nb_roundabouts", "prop_bike", "prop_bus_lane", "prop_slow_zone", "prop_one_way", "day", "holidays", "period", "target", "trip_id")
  }

  /** Builds lines on the network using a set of inter_stops.
    * each of them are used 1 to 3 times to create bus lines hence some of them are shared.
    * A bus line can't have duplicated inter-stops.
    * */
  protected def buildLines(nb: Int = 100, iStops: Array[InterStop]): Array[Line] = {
    val lines = new ArrayBuffer[Line]()
    val iStops_map_tmp = iStops.map(_ -> current().nextInt(1, 3)).toMap
    var iStops_map = collection.mutable.Map[InterStop, Int]() ++= iStops_map_tmp


    /** Build lines one by one */
    for (i <- 1 to nb) {
      /** Generate a random size for the line */
      val line_size = current().nextInt(12, 43)
      val line_Stops = new ArrayBuffer[InterStop]()

      /** Build the line only if there are enough free inter-stops */
      if (iStops_map.size >= line_size) {
        for (j <- 1 to line_size) {
          // Get an inter-point
          val availableIstops = iStops_map.keys.toArray
          var iStop = availableIstops(current().nextInt(0, availableIstops.length))
          while (line_Stops.contains(iStop)) {
            iStop = availableIstops(current().nextInt(0, availableIstops.length))
          }
          // Keep it if it is not already used in the line then update the number of time it can be reused
          // if the inter-stop has been overused, drop it from the map.
          line_Stops.append(iStop)
          iStops_map(iStop) = iStops_map(iStop) - 1
          if (iStops_map(iStop) <= 0) {
            iStops_map = iStops_map.filterNot(_._1 == iStop)
          }
        }
      }
      if (line_Stops.length == line_size) lines.append(Line(i, line_Stops.toArray))
    }
    lines.toArray
  }

  protected def trainRF(df: DataFrame, unit: String = "Seconds"): RandomForest = {
    val rf = smile.regression.randomForest(Formula.lhs("target"), df.drop("trip_id", "line_id"))
    logger.debug(s"""OOB RMSE : ${rf.error()} $unit \n """)
    rf
  }

  protected def predict(df: DataFrame, model: RandomForest, unit: String = "Seconds", agg: Boolean = false, agg_mode: Value = SUM): DataFrame = {

    val truth = df("target").toDoubleArray()
    val res = model.predict(df.drop("line_id", "trip_id"))
    val resZipped = res.zip(truth)

    if (agg) {
      val resZipped_tripID = resZipped.zip(df("trip_id").toDoubleArray()).map(x => (x._1._1, x._1._2, x._2))
      val trips = df("trip_id").toDoubleArray().distinct
      val tripsAgg = new ArrayBuffer[(Double, Double)]

      trips.foreach { t =>
        val trip = resZipped_tripID.filter(_._3 == t)

        val pred: Double = if (agg_mode != PROD) trip.map(_._1).sum else trip.map(_._1).product
        val truth: Double = if (agg_mode != PROD) trip.map(_._2).sum else trip.map(_._2).product

        tripsAgg.append((pred, truth))
      }
      val n1 = 1.0 / tripsAgg.length
      val mae = Math.abs(tripsAgg.map(r => r._1 - r._2).sum / tripsAgg.length)
      val rmse = validation.rmse(tripsAgg.map(_._1).toArray, tripsAgg.map(_._2).toArray)
      val mape = (n1 * (tripsAgg.map { r => Math.abs((r._2 - r._1) / r._2)}.sum)) * 100


      DataFrame.of(Array(Array(rmse, mae, mape)), s"""RMSE ($unit)""", s"""MAE ($unit)""", "MAPE (%)")

    }

    else {
      val n1 = 1.0 / df.size()
      val mae = Math.abs(resZipped.map(r => r._1 - r._2).sum / df.size())
      val rmse = validation.rmse(truth, res)
      val mape = (n1 * (resZipped.map { r => Math.abs((r._2 - r._1) / r._2)}.sum)) * 100

      DataFrame.of(Array(Array(rmse, mae, mape)), s"""RMSE ($unit)""", s"""MAE ($unit)""", "MAPE (%)")
    }


  }


  protected def kfold(k: Int = 10, lines: Array[Line], trips: Array[Trip], agg_mode: Value = SUM, unit: String): (Metrics, Metrics, Metrics) = {

    val microDS = trips.map(_.toMicroArray).reduce(_.union(_))
    val macroDS = trips.map(_.toMacroArray(agg_mode = agg_mode)).reduce(_.union(_))
    logger.debug(s"${microDS.summary()}")
    logger.debug(s"${macroDS.summary()}")

    val _micro = ArrayBuffer[DataFrame]()
    val _macro = ArrayBuffer[DataFrame]()
    val _compred = ArrayBuffer[DataFrame]()
    var _pct_known = 0.0

    for (i <- 1 to k) {
      logger.warn(s"--- Fold $i ---")
      val test_line = current().nextInt(1, lines.length - 1)
      val test_line_ips = lines.filter(_.id == test_line).head.interStops
      val all_lines_ips = lines.filter(_.id != test_line).flatMap {
        _.interStops
      }.distinct
      val commonIps = all_lines_ips.filter(test_line_ips.contains(_))

      val microDS_Train = createDF(microDS.toArray().filter(row => row(0) != test_line))
      val microDS_Test = createDF(microDS.toArray().filter(row => row(0) == test_line))

      val macroDS_Train = createDF(macroDS.toArray().filter(row => row(0) != test_line))
      val macroDS_Test = createDF(macroDS.toArray().filter(row => row(0) == test_line))

      logger.debug("MICRO")
      val microModel = trainRF(microDS_Train)
      logger.debug("MACRO")
      val macroModel = trainRF(macroDS_Train)


      _micro.append(predict(microDS_Test, microModel, unit, agg_mode = agg_mode))
      _macro.append(predict(macroDS_Test, macroModel, unit, agg_mode = agg_mode))
      _compred.append(predict(microDS_Test, microModel, unit, agg = true, agg_mode = agg_mode))
      _pct_known += commonIps.length.asInstanceOf[Double] / test_line_ips.length.asInstanceOf[Double]
    }

    val avg_micro = _micro.toArray.reduce(_.union(_)).summary()
    val avg_rmse_micro = avg_micro.column("avg")(0).asInstanceOf[Double]
    val avg_mae_micro = avg_micro.column("avg")(1).asInstanceOf[Double]
    val avg_mpe_micro = avg_micro.column("avg")(2).asInstanceOf[Double]

    val avg_macro = _macro.toArray.reduce(_.union(_)).summary()
    val avg_rmse_macro = avg_macro.column("avg")(0).asInstanceOf[Double]
    val avg_mae_macro = avg_macro.column("avg")(1).asInstanceOf[Double]
    val avg_mpe_macro = avg_macro.column("avg")(2).asInstanceOf[Double]

    val avg_compred = _compred.toArray.reduce(_.union(_)).summary()
    val avg_rmse_compred = avg_compred.column("avg")(0).asInstanceOf[Double]
    val avg_mae_compred = avg_compred.column("avg")(1).asInstanceOf[Double]
    val avg_mpe_compred = avg_compred.column("avg")(2).asInstanceOf[Double]

    (Metrics(avg_rmse_micro, avg_mae_micro, avg_mpe_micro, _pct_known / k),
      Metrics(avg_rmse_macro, avg_mae_macro, avg_mpe_macro, _pct_known / k),
      Metrics(avg_rmse_compred, avg_mae_compred, avg_mpe_compred, _pct_known / k)
    )

  }

  def main(args: Array[String]): Unit = {

    /** Nb of inter-stops to generate */
    val nb_inter_stops = 2000

    /** Nb of lines to generate on the network using already created inter-stops (some of them will be common to some lines) */
    val nb_lines = 110

    /** Build inter-stops */
    val iStops: ArrayBuffer[InterStop] = new ArrayBuffer[InterStop]()

    for (i <- 0 until nb_inter_stops) {
      iStops.append(InterStop())
    }

    /** Build lines and create foo trips */
    val lines = buildLines(nb = nb_lines, iStops.toArray)
    val trips_tt = generateTrips(lines, min_trips_per_line = 25, max_trips_per_line = 150, travelTime)
    val trips_rel = generateTrips(lines, min_trips_per_line = 25, max_trips_per_line = 150, reliability)

    val results_tt = kfold(k = 30, lines = lines, trips = trips_tt, unit = "Seconds")
    logger.info(s"MICRO results : ${results_tt._1}")
    logger.info(s"MACRO results : ${results_tt._2}")
    logger.info(s"COMPRED results : ${results_tt._3}")

    val results_rel = kfold(k = 30, lines = lines, trips = trips_rel, agg_mode = PROD, unit = "Percent [0;1]")
    logger.info(s"MICRO results : ${results_rel._1}")
    logger.info(s"MACRO results : ${results_rel._2}")
    logger.info(s"COMPRED results : ${results_rel._3}")

    logger.warn("END")
  }
}
