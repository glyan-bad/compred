package com.tritcorp.compred

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.hadoop.conf.Configuration
import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.udf

import java.sql.Timestamp

object Utils {

  val appConf: Config = ConfigFactory.load()
  val workDir: String = appConf.getString("app.working_dir")

  val conf: SparkConf = new SparkConf().setAppName("Compred").setMaster("local[*]")
  val ss: SparkSession = SparkSession.builder().config(conf).getOrCreate()
  ss.sparkContext.setLogLevel("WARN")

  val hadoopConfig: Configuration = ss.sparkContext.hadoopConfiguration
  hadoopConfig.set("fs.hdfs.impl", classOf[org.apache.hadoop.hdfs.DistributedFileSystem].getName)
  hadoopConfig.set("fs.file.impl", classOf[org.apache.hadoop.fs.LocalFileSystem].getName)


  /**
    * Data sources
    *
    */

  lazy val osm_enriched: DataFrame = try {
    ss.read.format("org.apache.spark.sql.parquet").load(appConf.getString("app.data.osm_enriched"))
  }


  /**
    * Filters applied on data upstream :
    * 0 < effective_speed < 70
    * 0 < travel_time
    * 0 <= dwell_time
    * incomplete trips removed
    * deviations data removed
    *
    */
  lazy val historical: DataFrame = ss.read.format("org.apache.spark.sql.parquet").load(appConf.getString("app.data.historical"))


  /**
    * Discretizers
    * note that we do not do one-hot encoding :
    * https://towardsdatascience.com/one-hot-encoding-is-making-your-tree-based-ensembles-worse-heres-why-d64b282b5769
    */


  val PeriodNum: Map[String, Int] = Map(
    "[23-01[" -> 1,
    "[01-03[" -> 2,
    "[03-06[" -> 3,
    "[06-07[" -> 4,
    "[07-08[" -> 5,
    "[08-09[" -> 6,
    "[09-10[" -> 7,
    "[10-11[" -> 8,
    "[11-12[" -> 9,
    "[12-13[" -> 10,
    "[13-14[" -> 11,
    "[14-16[" -> 12,
    "[16-17[" -> 13,
    "[17-18[" -> 14,
    "[18-19[" -> 15,
    "[19-20[" -> 16,
    "[20-23[" -> 17
  )

  val NumPeriod: Map[Int, String] = PeriodNum.map { case (k, v) => (v, k) }

  val Periods = NumPeriod.keySet.toList.sorted

  def period2Num = udf((p: String) => {
    PeriodNum.getOrElse(p, -1)
  })

  def num2Period = udf((p: Int) => {
    NumPeriod.getOrElse(p, "[]")
  })


  def holidays = udf((d: String) => {
    if ((d < "2018-09-03 00:00:00") || (d >= "2019-07-06 00:00:00" && d < "2019-09-01 00:00:00")) 1
    else if ((d >= "2018-10-20 00:00:00" && d < "2018-11-05 00:00:00") || (d >= "2019-10-19 00:00:00" && d < "2019-11-04 00:00:00")) 2
    else if ((d >= "2018-12-22 00:00:00" && d < "2019-01-07 00:00:00") || (d >= "2019-12-21 00:00:00" && d < "2020-01-06 00:00:00")) 3
    else if ((d >= "2019-02-09 00:00:00" && d < "2019-02-25 00:00:00") || (d >= "2020-02-08 00:00:00" && d < "2020-03-09 00:00:00")) 4
    else if (d >= "2019-04-06 00:00:00" && d < "2019-04-23 00:00:00") 5
    else 6
  })


  val MaxSpeed: Double = 70.0
  val MinSpeed: Double = 1.0
  val MinTravelTime: Int = 5
  val MinDist: Int = 10

  def speedCalcFloat = udf((dist: Int, travel_time: Int, orig: Int) => {
    var res = 0.0
    if (travel_time >= MinTravelTime && dist >= MinDist) {
      res = "%.1f".format((dist.toDouble / travel_time.toDouble) * 3.6).toDouble
    }
    res = if (res > MaxSpeed || res <= MinSpeed) {
      if (orig > MinSpeed && orig < MaxSpeed) orig.toDouble else 0.0
    } else {
      res
    }
    res
  })


  def timeStampFromString(instant: String): Long = {
    Timestamp.valueOf(instant).getTime
  }


  def travelTimeCalc = udf((origStart: String, destArr: String) => {
    (timeStampFromString(destArr) - timeStampFromString(origStart)) / 1000
  })


}
